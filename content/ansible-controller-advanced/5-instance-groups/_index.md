+++
title = "Automation controller instance groups"
weight = 5
+++

Automation controller clustering allows you to easily add capacity to your controller infrastructure by adding instances. To enable more control over which node is running a job, Tower 3.2 saw the introduction of the **Instance Groups** feature. Instance groups allow you to organize your cluster nodes into groups. In turn Jobs can be assigned to Instance Groups by configuring the Groups in Organizations, Inventories or Job Templates.

{{% notice tip %}}
The order of priority is **Job Template > Inventory > Organization**. So instance groups configured in Job Templates take precedence over those configured in Inventories, which take precedence over Organization defaults.
{{% /notice %}}

Some things to keep in mind about instance groups:

- A fresh clustered installation always comes with the `controlplane` instance group which has to contain at least one instance

- An instance group `default` is generated with all nodes capable of running jobs by default and can not be deleted.
- Nodes can be in one or more instance groups.
- Nodes in an instance group share a job queue.
- Controller instances can’t have the same name as an instance group.

Instance groups allow some pretty cool setups, e.g. you could have some nodes shared over the whole cluster (by putting them into all groups) but then have other nodes that are dedicated to one group to reserve some capacity.

{{% notice warning %}}
The base `controlplane` group does housekeeping for all groups so the node count of this group has to **scale with your overall cluster load**, even if these nodes are not used to run Jobs.
Housekeeping encompasses for example processing of events from all jobs.
{{% /notice %}}

Regarding the `default` group: If a resource is not associated with any other instance group, one of the nodes from the `default` group will run the Job.

## Instance group setup

Having the introduction out of the way, let’s get back to our lab and give instance groups a try.

In a basic cluster setup like ours you just have the `controlplane` and `default` groups. So let’s go and setup two new instance groups:

- In **Instance Groups** add a new group by clicking the ![Add](../../images/blue_add_dd.png?classes=inline) icon and then **Add instance group**
- Name the new group **dev**
- **Save**
- Go to the **Instances** tab of the new Instances Group and add node **autoctl2** by clicking the **Associate** button.

Do the same to create a the new group **prod** with instance **autoctl3**

Go back to the **Instance Groups** view, you should now have the following setup:

- All instances are in the **controlplane** and **default** instance group
- Two more groups (**prod** and **dev**) with one instances each

{{% notice tip %}}
We’re using the internal names of the controller nodes here.
{{% /notice %}}

{{% notice warning %}}
This is not best practice, it’s just for the sake of this lab! Any jobs that are launched targeting an instance group without active nodes will be stuck in a waiting state until instances become available. So one-instance groups are never a good idea.
{{% /notice %}}

## Verify instance groups

You can check your instance groups in a number of ways.

### Via the Web UI

You have configured the groups here, open the URL

```raw
https://autoctl1.<LABID>.<SUBDOMAIN>.opentlc.com/#/instance_groups
```

in your browser.

In the **Instance Groups** overview all instance groups are listed with details of the group itself like number of instances in the group, running jobs and total jobs. Like you’ve seen before the current capacity of the instance groups is shown in a live view, thus providing a quick insight if there are capacity problems.

### Via the API

For a change query the API to get this information. Either use the browser to access the URL (you might have to login to the API again):

```raw
https://https://autoctl1.<LABID>.<SUBDOMAIN>.opentlc.com/api/v2/instance_groups/
```

or use curl to access the API from the command line in your VSCode terminal:

```bash
curl -s -k -u admin:'<YOUR_SECRET_PASSWORD>' https://autoctl1.<LABID>.<SUBDOMAIN>.opentlc.com/api/v2/instance_groups/ | jq
```

{{% notice tip %}}
The curl command has to be on one line. Do _not_ forget or overlook the final slash at the end of the URL, it is relevant!
{{% /notice %}}

## Deactivating automation controller instances

While in the **Instances Groups** overview in the web UI click the **dev** group. In the next view change to the **Instances** tab, at the right you’ll see a slide button for each controller instance (only one in this case).

- The button should be set to **On** meaning "active". Clicking it would deactivate the corresponding instance and would prevent that further jobs are assigned to it.

- Running jobs on an instance which is set to **Off** are finished in a normal way.

- The slider **Capacity Adjustment** can change the amount of forks scheduled on an instance. This way it is possible to influence in which ratio the jobs are assigned.
