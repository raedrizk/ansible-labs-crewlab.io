+++
title = "Introduction to automation controller clustering"
weight = 2
+++

With version 3.1 Ansible Tower introduced clustering, replacing the redundancy solution configured with active-passive nodes. Clustering is balancing load between controller nodes. Each controller instance is able to act as an entry point for UI and API access. All configuration settings and data is stored in a shared PostgreSQL database which all cluster nodes are using. This eliminates the need to synchronize data or configuration settings between the cluster nodes.

{{% notice tip %}}
Using a load balancer in front of the controller nodes is possible, but optional because an automation controller cluster can be accessed via all controller instances.
{{% /notice %}}

Each instance in a controller cluster expands the cluster’s capacity to execute jobs. Jobs can and will run anywhere in the cluster by finding the least utilized node and other criteria explained later in this lab.

## Access the Controller Web UI

For the first contact to your cluster open your browser and login to the first controller node's web UI as user `admin` with the password provided.

```raw
https://autoctl1.<LABID>.<SUBDOMAIN>.opentlc.com/
```

{{% notice warning %}}
Replace **LABID** and **SUBDOMAIN** with your values!
{{% /notice %}}

Just from the web UI you wouldn’t know you’ve got a controller cluster at your hands. To learn more about your cluster and its state, navigate to the **Administration** menu and open **Instance Groups**. Here you will get an overview of the cluster by Instance Groups. Explore the information provided, of course there is no capacity used yet and no Jobs have run.

Right now we only have the two default **Instance Groups**:

 * **default**: holding all nodes able to execute automation jobs
 * **controlplane**: for controller and hybrid nodes, more on this in the automation mesh chapter

 When you get more groups, from this view you will see how the instances are distributed over the groups. Click on **controlplane**.

To dig deeper click on the **Instances** tab to get more information about the instances assigned to a group. In the instances view you can toggle nodes off/online and adjust the number of forks (don't do this now). You’ll learn more about this later.

## Access your controller cluster via command line

You can also get information about your cluster on the command line. Log in to your **VS Code** again if you closed it by opening this URL in your browser:

```raw
https://bastion.<LABID>.<SUBDOMAIN>.opentlc.com
```

Your VSCode session is running on your bastion host. Again if not still open, open a terminal by clicking **Terminal**, **New Terminal** in the menu bar.

A terminal window opens at the bottom, now SSH into your automation controller node 1 (change **\<LABID>**):

```bash
ssh autoctl1.<LABID>.internal
```

In the terminal run the following command:

```bash
$ sudo -i
$ awx-manage list_instances
[controlplane capacity=48 policy=100%]
        autoctl1.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:30"
        autoctl2.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:30"
        autoctl3.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:29"

[default capacity=48 policy=100%]
        autoctl1.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:30"
        autoctl2.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:30"
        autoctl3.<LABID>.internal capacity=16 node_type=hybrid version=4.2.0 heartbeat="2022-08-01 12:21:29"
        exec1.<LABID>.internal capacity=16 node_type=execution version=ansible-runner-2.2.1 heartbeat="2022-08-24 12:21:44"
```

{{% notice note %}}
Your exact hostnames and timestamps will differ, of course!
{{% /notice %}}

So what we’ve got is a three-node controller cluster with one execution node, no surprises here. In addition the command tells us the capacity (maximum number of job slices or concurrent jobs) per node and for the instance groups. Here the capacity value of 16 is allocated to all nodes. We can also see an automation mesh node which we will cover in more detail in the related chapter.

{{% notice warning %}}
Please logout of the automation controller node so in the terminal you are a regular user on the `bastion` host again!
{{% /notice %}}

{{% notice tip %}}
The **awx-manage** (formerly tower-manage) utility can be used to manage a lot of the more internal aspects of the automation controller.
You can e.g. use it to clean up old data, for token and session management and for cluster management.
Just call **awx-manage** without parameters to see all possible subcommands.
{{% /notice %}}
